package com.app;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.app.controllers.AwsController;

@Component
public class CleanImage {
	
	@Autowired
	AwsController awsController;

	private static final Logger log = LoggerFactory.getLogger(CleanImage.class);

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(fixedRate=60*60*1000)
	public void task() throws IOException {
		awsController.delete();
		log.info("Delete images {}", dateFormat.format(new Date()));
	}
}
