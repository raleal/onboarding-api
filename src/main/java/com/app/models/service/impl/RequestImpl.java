package com.app.models.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.app.models.dao.IRequestDao;
import com.app.models.dao.IUserDao;
import com.app.models.entity.Auditory;
import com.app.models.entity.Image;
import com.app.models.entity.Request;
import com.app.models.entity.RequestImage;
import com.app.models.entity.User;
import com.app.models.entity.dto.RequestRs;
import com.app.models.service.IAuditoryService;
import com.app.models.service.IRequestImageService;
import com.app.models.service.IRequestService;

@Service
public class RequestImpl implements IRequestService {

    @Autowired
    private IRequestDao iRequestDao;

    @Autowired
    private IUserDao iUserDao;

    @Autowired
    private IAuditoryService iAuditoryService;
    
    @Autowired
    private IRequestImageService iRequestImageService;

    @Autowired
    private Environment env;

    @Override
    public Request save(Request request, Authentication authentication) {
    	 
        if(Objects.nonNull(request.getRequestImage())) {
			List<RequestImage> requestImage = new ArrayList<RequestImage>();
			for(RequestImage ri : request.getRequestImage()) {
				Image image = ri.getImage();
				RequestImage rImage = new RequestImage();
				rImage.setImage(image);;
				rImage = iRequestImageService.save(rImage);
				requestImage.add(rImage);
			}
			
			request.setRequestImage(requestImage);
		}
        
        request = iRequestDao.save(request);
        
        Integer numRq = Integer.valueOf(env.getProperty("number.request"));
        request.setControlNumber(String.valueOf(numRq + request.getId()));
        
        if (Objects.nonNull(request)) {
            User user = iUserDao.findByUsername(authentication.getPrincipal().toString());
            Auditory auditory = Auditory.builder().module("Request").action("Save").user(user).build();
            iAuditoryService.save(auditory);
        }
        return iRequestDao.save(request);
    }

    @Override
    public List<Request> listRequest(String filter, String identification, Authentication authentication) {

        List<Request> listResponse = new ArrayList<>();

        List<Request> listRequest = new ArrayList<>();

        if (Objects.nonNull(filter) || Objects.nonNull(identification)) {
            if ((Objects.nonNull(filter) && !filter.equals("")) && (Objects.nonNull(identification) &&
                    !identification.equals(""))) {
                listRequest = iRequestDao.listRequestGroupFilterIdentification(filter, identification);
            } else if (Objects.nonNull(filter) && !filter.equals("")) {
                listRequest = iRequestDao.listRequestGroupFilter(filter);
            } else {
                listRequest = iRequestDao.listRequestGroupIdentification(identification);
            }

        } else {
            listRequest = iRequestDao.listRequestGroup();
        }

        for (Request request : listRequest) {
            List<RequestRs> listRs = new ArrayList<>();
            List<Request> listReq = new ArrayList<>();
            if (Objects.nonNull(filter) && !filter.equals("")) {
                listReq = iRequestDao.listRequestIdDocumentFilter(request.getIdentificationDocument(), filter);
            } else {
                listReq = iRequestDao.listRequestIdDocument(request.getIdentificationDocument());
            }
            for (Request rq : listReq) {
                RequestRs requestRs = new RequestRs();
                BeanUtils.copyProperties(rq, requestRs);
                listRs.add(requestRs);

                String statusRequest = rq.getBankResponse();

                switch (statusRequest) {
                    case "successfull":
                        request.setPassed(true);
                        break;
                    case "rejected":
                        request.setRejected(true);
                        break;
                    case "revision":
                        request.setRevision(true);
                        break;
                }

            }
            request.setRequests(listRs);
            listResponse.add(request);

        }

        if (!listResponse.isEmpty()) {
            User user = iUserDao.findByUsername(authentication.getPrincipal().toString());
            Auditory auditory = Auditory.builder().module("Request").action("List").user(user).build();
            iAuditoryService.save(auditory);
        }
        return listResponse;
    }

    @Override
    public Request find(Long id, Authentication authentication) {
        Request request = iRequestDao.findById(id).orElse(null);
        if (Objects.nonNull(request)) {
            User user = iUserDao.findByUsername(authentication.getPrincipal().toString());
            Auditory auditory = Auditory.builder().module("Request").action("Find").user(user).build();
            iAuditoryService.save(auditory);
        }
        return request;
    }

}

