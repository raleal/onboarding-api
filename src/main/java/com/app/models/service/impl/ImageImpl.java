package com.app.models.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.app.models.dao.IImageDao;
import com.app.models.entity.Image;
import com.app.models.service.IImageService;

@Service
public class ImageImpl implements IImageService {
	
	@Autowired
	private IImageDao iImageDao;
	
	@Autowired
    private Environment env;

	@Override
	public String save(MultipartFile upload) throws IOException {
		// TODO Auto-generated method stub
		AWSCredentials credentials = new BasicAWSCredentials(
				env.getProperty("aws.access.key"), 
				env.getProperty("aws.secret.key")
				);
		
		AmazonS3 s3client = AmazonS3ClientBuilder
				  .standard()
				  .withCredentials(new AWSStaticCredentialsProvider(credentials))
				  .withRegion(Regions.US_EAST_1)
				  .build();
		
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(upload.getSize());
		
		String uniqueFilename = UUID.randomUUID().toString() + "_" + upload.getOriginalFilename().replaceAll("\\s+","");
		
		PutObjectResult putObjectResult = s3client.putObject(env.getProperty("aws.bucket.name"), uniqueFilename, upload.getInputStream(), metadata);
		
		Image image = new Image();
		
		image.setKeyFile(uniqueFilename);
		
		Byte[] byteObjects = new Byte[upload.getBytes().length];

	    int i = 0;

	    for (byte b : upload.getBytes()){
	        byteObjects[i++] = b;
	    }
	    
	    image.setImage(byteObjects);
	    image.setETag(putObjectResult.getETag());
	    
	    image = iImageDao.save(image);
	    
		return image.getId().toString();
	}

	@Override
	public List<String> list() {
		// TODO Auto-generated method stub
		
		List<String> list = new ArrayList<String>();
		
		AWSCredentials credentials = new BasicAWSCredentials(
				env.getProperty("aws.access.key"), 
				env.getProperty("aws.secret.key")
				);
		
		AmazonS3 s3client = AmazonS3ClientBuilder
				  .standard()
				  .withCredentials(new AWSStaticCredentialsProvider(credentials))
				  .withRegion(Regions.US_EAST_1)
				  .build();
		
		ObjectListing objectListing = s3client.listObjects(env.getProperty("aws.bucket.name"));
		
		for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
			list.add(os.getKey());
		}
		
		return list;
	}

	@Override
	public List<Image> listImageExpired() {
		// TODO Auto-generated method stub
		return iImageDao.listImageExpired();
	}

	@Override
	public List<Image> listImageNotRelated() {
		// TODO Auto-generated method stub
		return iImageDao.listImageNotRelated();
	}

	@Override
	public void deleteImage() {
		// TODO Auto-generated method stub
		List<Image> images = new ArrayList<Image>();
		images.addAll(listImageExpired());
		images.addAll(listImageNotRelated());
		
		AWSCredentials credentials = new BasicAWSCredentials(
				env.getProperty("aws.access.key"), 
				env.getProperty("aws.secret.key")
				);
		
		AmazonS3 s3client = AmazonS3ClientBuilder
				  .standard()
				  .withCredentials(new AWSStaticCredentialsProvider(credentials))
				  .withRegion(Regions.US_EAST_1)
				  .build();
		
		for(Image image : images) {
			s3client.deleteObject(env.getProperty("aws.bucket.name"),image.getKeyFile());
		}
		
	}


}

