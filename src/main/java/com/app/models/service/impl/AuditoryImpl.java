package com.app.models.service.impl;

import com.app.models.dao.IAuditoryDao;
import com.app.models.dao.IEconomicActivityDao;
import com.app.models.entity.Auditory;
import com.app.models.entity.EconomicActivity;
import com.app.models.service.IAuditoryService;
import com.app.models.service.IUtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuditoryImpl implements IAuditoryService {
	
	@Autowired
	private IAuditoryDao iAuditoryDao;

	@Override
	public List<Auditory> listAuditory() {
		return iAuditoryDao.listAuditory();
	}

	@Override
	public void save(Auditory auditory) {
		iAuditoryDao.save(auditory);
	}

}

