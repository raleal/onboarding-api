package com.app.models.service.impl;

import java.util.List;

import com.app.models.dao.IEconomicActivityDao;
import com.app.models.entity.EconomicActivity;
import com.app.models.service.IUtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UtilsImpl implements IUtilsService {
	
	@Autowired
	private IEconomicActivityDao iEconomicActivityDao;

	@Override
	public List<EconomicActivity> listEconomicActivity(String term) {
		return iEconomicActivityDao.listEconomicActivity(term);
	}

}

