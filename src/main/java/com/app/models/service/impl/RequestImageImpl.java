package com.app.models.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.models.dao.IRequestImageDao;
import com.app.models.entity.RequestImage;
import com.app.models.service.IRequestImageService;

@Service
public class RequestImageImpl implements IRequestImageService {

    @Autowired
    private IRequestImageDao iRequestImageDao;

	@Override
	public RequestImage save(RequestImage requestImage) {
		// TODO Auto-generated method stub
		return iRequestImageDao.save(requestImage);
	}

   
}

