package com.app.models.service;

import com.app.models.entity.EconomicActivity;
import com.app.models.entity.Request;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface IRequestService {

	public Request save(Request request, Authentication authentication);

	public List<Request> listRequest(String filter, String identification, Authentication authentication);

	public Request find(Long id, Authentication authentication);
	
}
