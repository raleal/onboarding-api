package com.app.models.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.app.models.entity.Image;

public interface IImageService {

	public String save(MultipartFile upload) throws IOException ;
	
	public List<String> list();
	
	public List<Image> listImageExpired();
	
	public List<Image> listImageNotRelated();
	
	public void deleteImage();
	
}
