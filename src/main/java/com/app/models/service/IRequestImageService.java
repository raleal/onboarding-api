package com.app.models.service;

import com.app.models.entity.RequestImage;

public interface IRequestImageService {

	public RequestImage save(RequestImage requestImage);
	
}
