package com.app.models.service;

import java.util.List;

import com.app.models.entity.EconomicActivity;

public interface IUtilsService {

	public List<EconomicActivity> listEconomicActivity(String term);
	
}
