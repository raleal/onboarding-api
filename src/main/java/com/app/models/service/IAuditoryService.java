package com.app.models.service;

import com.app.models.entity.Auditory;
import com.app.models.entity.EconomicActivity;

import java.util.List;

public interface IAuditoryService {

	public List<Auditory> listAuditory();

	public void save(Auditory auditory);
	
}
