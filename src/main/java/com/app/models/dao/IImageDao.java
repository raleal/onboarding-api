package com.app.models.dao;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.app.models.entity.Image;

public interface IImageDao extends CrudRepository<Image, Long> {
	
	 @Query(value = "select * from  image i where i.date < DATE_SUB(NOW(),INTERVAL 1 HOUR)",
	            nativeQuery = true)
	    public List<Image> listImageExpired();
	 
	 @Query(value = "SELECT * FROM image i WHERE NOT EXISTS ( SELECT * FROM request_image ri WHERE ri.image_id = i.id )",
			 nativeQuery = true)
	    public List<Image> listImageNotRelated();

}
