package com.app.models.dao;


import com.app.models.entity.Auditory;
import com.app.models.entity.EconomicActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IAuditoryDao extends CrudRepository<Auditory, Long> {

    @Query(value = "SELECT * FROM auditory a ORDER BY a.id DESC",
            nativeQuery = true)
    public List<Auditory> listAuditory();

}
