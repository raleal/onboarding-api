package com.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.app.models.entity.RequestImage;

public interface IRequestImageDao extends CrudRepository<RequestImage, Long>{

}
