package com.app.models.dao;

import com.app.models.entity.EconomicActivity;
import com.app.models.entity.Request;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IRequestDao extends CrudRepository<Request, Long> {

    @Query(value = "SELECT * FROM request", nativeQuery = true)
    public List<Request> listRequest();

    @Query(value = "SELECT *, r.id FROM request r GROUP BY r.identification_document ORDER BY r.id DESC", nativeQuery = true)
    public List<Request> listRequestGroup();

    @Query(value = "SELECT *, r.id FROM request r WHERE r.bank_response = ?1 GROUP BY r.identification_document, r.id ORDER BY r.id DESC", nativeQuery = true)
    public List<Request> listRequestGroupFilter(String filter);

    @Query(value = "SELECT *, r.id FROM request r WHERE r.identification_document = ?1 GROUP BY r.identification_document, r.id ORDER BY r.id DESC", nativeQuery = true)
    public List<Request> listRequestGroupIdentification(String identification);

    @Query(value = "SELECT *, r.id FROM request r WHERE r.bank_response = ?1 AND r.identification_document = ?2 GROUP BY r.identification_document, r.id ORDER BY r.id DESC", nativeQuery = true)
    public List<Request> listRequestGroupFilterIdentification(String filter, String identification);

    @Query(value = "SELECT * FROM request r WHERE r.identification_document = ?1 ORDER BY r.id DESC", nativeQuery = true)
    public List<Request> listRequestIdDocument(String idDocument);

    @Query(value = "SELECT * FROM request r WHERE r.identification_document = ?1 AND r.bank_response = ?2 ORDER BY r.id DESC", nativeQuery = true)
    public List<Request> listRequestIdDocumentFilter(String idDocument, String filter);

}
