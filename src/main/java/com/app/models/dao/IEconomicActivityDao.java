package com.app.models.dao;

import com.app.models.entity.EconomicActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IEconomicActivityDao extends CrudRepository<EconomicActivity, Long>{

	@Query(value = "SELECT * FROM economic_activity ea WHERE ea.description_es like %?1%",
			nativeQuery = true)
	public List<EconomicActivity> listEconomicActivity(String term);

}
