package com.app.models.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.app.models.entity.dto.RequestRs;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "request")
public class Request implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String identificationDocument;

	private Date issued;

	private Date expires;

	private String name;

	private String lastName;

	private Date dateBirth;

	private String placeBirth;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="economic_activity_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private EconomicActivity EconomicActivity;

	private String email;

	private String urlSelfie;

	private String urlIdentificationDocument;

	private Integer satisfactionPercentage;
	
	private String bankResponse;

	private String controlNumber;

	private Date updatedDate;

	private Boolean passed;

	private Boolean rejected;

	private Boolean revision;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "request_id")
	private List<RequestImage> requestImage;

	@Transient
	private List<RequestRs> requests;

	@PrePersist
	public void prePersist() {
		issued = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(issued);
		calendar.add(Calendar.HOUR_OF_DAY, 1);
		expires = calendar.getTime();
	}
 
    @PreUpdate
    public void preUpdate() {
    	updatedDate = new Date();
    }

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
