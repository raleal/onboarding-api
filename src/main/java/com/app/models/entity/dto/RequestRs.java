package com.app.models.entity.dto;

import com.app.models.entity.EconomicActivity;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class RequestRs implements Serializable {

	private Long id;

	private String identificationDocument;

	private Date issued;

	private Date expires;

	private String name;

	private String lastName;

	private Date dateBirth;

	private String placeBirth;

	private EconomicActivity EconomicActivity;

	private String email;

	private String urlSelfie;

	private String urlIdentificationDocument;

	private Integer satisfactionPercentage;
	
	private String bankResponse;

	private String controlNumber;

	private Date updatedDate;

}
