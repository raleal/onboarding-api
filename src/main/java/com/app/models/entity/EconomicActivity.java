package com.app.models.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "economic_activity")
public class EconomicActivity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long naics;

	private Integer risk;

	private String description;

	private String descriptionEs;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
