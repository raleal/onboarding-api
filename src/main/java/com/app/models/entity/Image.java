package com.app.models.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "image")
public class Image implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String keyFile;
	
	private String eTag;
	
	@Lob
	private Byte[] image;
	
	private Date date;
	
	@PrePersist
	public void prePersist() {
		date = new Date();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
