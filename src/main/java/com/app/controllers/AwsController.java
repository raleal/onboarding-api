package com.app.controllers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.app.models.service.IImageService;

@RestController
@RequestMapping("/api/aws")
public class AwsController {
	
	@Autowired
	private IImageService iImageService;
	

	@PostMapping(value = "/upload")
	public Map<String, String> upload(@RequestParam("upload") MultipartFile upload, 
			HttpServletRequest rq, HttpServletResponse rs)
			throws IOException {
		
		HashMap<String, String> response = new HashMap<>();
		
		if (!upload.isEmpty()) {		
			
			response.put("id", iImageService.save(upload));
			return response;
			
		} else {
			
			rs.sendError(HttpServletResponse.SC_BAD_REQUEST,"Do not Attach the File");
			response.put("error","Do not Attach the File");
			return null;
			
		}		

	}
	
	@GetMapping(value = "/list")
    public List<String> list() {
		
		return iImageService.list();
		
    }
		
	@GetMapping(value = "/delete")
    public void delete() throws IOException {
				
		iImageService.deleteImage();
		
	}

}
