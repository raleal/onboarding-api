package com.app.controllers;

import com.app.models.entity.EconomicActivity;
import com.app.models.entity.Request;
import com.app.models.service.IRequestService;
import com.app.models.service.IUtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/api/request")
public class RequestController {

    @Autowired
    private IRequestService iRequestService;

    @RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public Request save(@RequestBody Request request, HttpServletRequest httpServletRequest,
                        HttpServletResponse httpServletResponse, Authentication authentication) {
        return iRequestService.save(request, authentication);
    }

    @GetMapping(value = "/list")
    public List<Request> listRequest(@RequestParam(required = false) String filter, @RequestParam(required = false)
            String identification, Authentication authentication) {
        return iRequestService.listRequest(filter, identification, authentication);
    }

    @GetMapping(value = "/{requestId}")
    public Request request(@PathVariable(value = "requestId") Long requestId, Authentication authentication) {
        return iRequestService.find(requestId, authentication);
    }

}
