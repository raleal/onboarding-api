package com.app.controllers;

import com.app.models.entity.Auditory;
import com.app.models.entity.EconomicActivity;
import com.app.models.service.IAuditoryService;
import com.app.models.service.IUtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/api/auditory")
public class AuditoryController {

    @Autowired
    private IAuditoryService iAuditoryService;

    @GetMapping(value = "/list")
    public List<Auditory> listAuditory(HttpServletResponse response) {
        return iAuditoryService.listAuditory();
    }

}
