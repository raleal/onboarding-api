package com.app.controllers;

import com.app.models.entity.EconomicActivity;
import com.app.models.service.IUtilsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/api/utils")
public class UtilsController {

    @Autowired
    private IUtilsService iUtilsService;

    @GetMapping(value = "/economicActivity/{term}")
    public List<EconomicActivity> listEconomicActivity(@PathVariable(value = "term") String term, HttpServletResponse response) {
        return iUtilsService.listEconomicActivity(term);
    }

}
