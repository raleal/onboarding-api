package com.app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class BnpApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BnpApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {	}
}
